import { createReducer, on } from '@ngrx/store';

import { AuthToken, UserProfile } from '../models';
import { AuthApiActions, AuthActions } from '../actions';

export const statusFeatureKey = 'status';

export interface State {
  authToken: AuthToken;
  loggedInUser: UserProfile;
}

export const initialState: State = {
  // tslint:disable-next-line:max-line-length
  authToken: JSON.parse('{"first_name":"Eatons","last_name":"Russo","username":"vulputate.velit.eu@nascetur.com", "_id":"5ca8abe94c224c44c7ec3e6e", "accessToken":"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhMSI6InBsYWNlaG9sZGVyIiwiaWF0IjoxNTg2NDQ0MTAxLCJleHAiOjE1ODY0ODczMDEsImF1ZCI6Imh0dHA6Ly9iaWxhdGVyYWwuY2EiLCJpc3MiOiJCaWxhdGVyYWwgRGVzaWducyIsInN1YiI6ImFkbWluQGJpbGF0ZXJhbC5jYSJ9.JObMI9WSM_aNvwUu9YehyQmqVlwO_ANCNaZzLu6p46pUQv03faEpuXkKRUsV1ugz00hzstxES0rA1Dvq8LEzrg","expiresIn":"3600","tokenType":"Bearer"}'),
  loggedInUser: {
    id: '5ca8abe94c224c44c7ec3e6e',
    username: 'Bob',
    avatarUrl: './assets/images/dev.png'
  }
};

export const reducer = createReducer(
  initialState,

  on(AuthApiActions.loginSuccess, (state, { authToken }) => ({
    ...state,
    authToken
  })),

  on(AuthActions.lock, (state) => ({
    ...state,
    // authToken: undefined
  })),

  on(AuthActions.logout, () => initialState)
);

export const getAuthToken = (state: State) => state.authToken;
export const getLoggedInUser = (state: State) => state.loggedInUser;
