import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { TranslateModule } from '@ngx-translate/core';
const DIALOGS = [
  AlertDialogComponent,
  ConfirmDialogComponent
];

@NgModule({
  imports: [
    TranslateModule,
    CommonModule,
    MatButtonModule,
    MatDialogModule
  ],
  declarations: DIALOGS,
  entryComponents: DIALOGS,
  exports: DIALOGS,
})
export class DialogModule { }
