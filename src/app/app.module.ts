import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CoreModule } from '@app/core';
import { i18nMultiModuleLoaderFactory } from '@app/core/i18n';
import { AppLayoutModule } from '@app/layout';
import { AuthModule, AuthConfiguration } from '@app/auth';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '@app/env';
import { StoreModule } from '@ngrx/store';
import { reducers } from '@app/core/store';

const authConfig: AuthConfiguration = {
  loginURL: 'login',
  loginApiURL: 'auth/login',
  headerName: 'Authorization',
  skipWhenExpired: true,
  whitelistedDomains: [
    environment.apiBaseUrl
  ],
  blacklistedRoutes: [
    'auth/login'
  ]
};

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,

    StoreModule.forRoot(reducers),
    // Instrumentation must be imported after importing StoreModule (config is optional)
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: i18nMultiModuleLoaderFactory,
        deps: [HttpClient]
      }
    }),

    AppRoutingModule,
    CoreModule.forRoot(),
    AuthModule.forRoot(authConfig),
    AppLayoutModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
