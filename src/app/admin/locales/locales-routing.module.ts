import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocalesPageComponent } from './containers';

const routes: Routes = [
  {
    path: '',
    component: LocalesPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocalesRoutingModule { }
