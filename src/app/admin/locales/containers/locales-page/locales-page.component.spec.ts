import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalesPageComponent } from './locales-page.component';

describe('LocalesPageComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LocalesPageComponent]
    })
      .compileComponents();
  }));

  function setup() {
    const fixture: ComponentFixture<LocalesPageComponent> = TestBed.createComponent(LocalesPageComponent);
    const component: LocalesPageComponent = fixture.componentInstance;
    fixture.detectChanges();

    return { fixture, component };
  }

  it('should create', () => {
    const { component } = setup();

    expect(component).toBeTruthy();
  });
});
