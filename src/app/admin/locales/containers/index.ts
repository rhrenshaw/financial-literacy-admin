import { LocalesPageComponent } from './locales-page/locales-page.component';

export const CONTAINER_COMPONENTS = [
  LocalesPageComponent
];

export {
  LocalesPageComponent
};
