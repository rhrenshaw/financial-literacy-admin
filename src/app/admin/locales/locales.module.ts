import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { LocalesRoutingModule } from './locales-routing.module';
import { CONTAINER_COMPONENTS } from './containers';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    LocalesRoutingModule
  ],
  declarations: [
    CONTAINER_COMPONENTS
  ]
})
export class LocalesModule { }
