import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { ContactModel } from '../models';
import { ContactActions } from '../actions';

export interface State extends EntityState<ContactModel> {
}

export function sortByUsername(a: ContactModel, b: ContactModel): number {
  if (!a.last_name || !b.last_name) {
    return 0;
  }

  return a.last_name.localeCompare(b.last_name);
}

export const adapter: EntityAdapter<ContactModel> = createEntityAdapter<ContactModel>({
  selectId: (contact: ContactModel) => contact._id,
  sortComparer: sortByUsername,
});

export const initialState: State = adapter.getInitialState({});

export const reducer = createReducer(
  initialState,

  on(
    ContactActions.findContactSuccess,
    (state, { contacts }) => {
      return adapter.addMany(contacts, state);
    }
  ),

  on(
    ContactActions.findContactByIdSuccess,
    (state, { contact }) => {
      return adapter.addOne(contact, state);
    }
  )
);
