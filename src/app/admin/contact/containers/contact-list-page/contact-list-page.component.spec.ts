import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule  } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { UsersPageComponent } from './contact-list-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';

describe('UsersPageComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCheckboxModule, MatIconModule, MatTableModule, MatMenuModule, Router,
        TranslateModule.forRoot()
      ],
      declarations: [UsersPageComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  function setup() {
    const fixture: ComponentFixture<UsersPageComponent> = TestBed.createComponent(UsersPageComponent);
    const component: UsersPageComponent = fixture.componentInstance;
    fixture.detectChanges();

    return { fixture, component };
  }

  it('should create', () => {
    const { component } = setup();

    expect(component).toBeTruthy();
  });
});
