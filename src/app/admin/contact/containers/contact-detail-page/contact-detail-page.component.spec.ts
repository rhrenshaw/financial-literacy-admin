import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailPageComponent } from './contact-detail-page.component';

describe('UsersPageComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserDetailPageComponent]
    })
      .compileComponents();
  }));

  function setup() {
    const fixture: ComponentFixture<UserDetailPageComponent> = TestBed.createComponent(UserDetailPageComponent);
    const component: UserDetailPageComponent = fixture.componentInstance;
    fixture.detectChanges();

    return { fixture, component };
  }

  it('should create', () => {
    const { component } = setup();

    expect(component).toBeTruthy();
  });
});
