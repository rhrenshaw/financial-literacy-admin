import { UsersCreatePageComponent } from './contact-create-page/contact-create-page.component';
import { UserDetailPageComponent } from './contact-detail-page/contact-detail-page.component';
import { UsersPageComponent } from './contact-list-page/contact-list-page.component';

export const CONTAINERS = [
  UsersCreatePageComponent,
  UserDetailPageComponent,
  UsersPageComponent
];

export {
  UsersCreatePageComponent,
  UserDetailPageComponent,
  UsersPageComponent
};
