import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  UsersPageComponent,
  UsersCreatePageComponent,
  UserDetailPageComponent
} from './containers';

const routes: Routes = [
  {
    path: '',
    component: UsersPageComponent
  },
  {
    path: 'create',
    component: UsersCreatePageComponent
  },
  {
    path: ':id',
    component: UserDetailPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
