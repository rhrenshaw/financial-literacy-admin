import { HierarchyEffect } from './hierarchy.effect';

export const EFFECTS = [
  HierarchyEffect
];

export {
  HierarchyEffect
};
