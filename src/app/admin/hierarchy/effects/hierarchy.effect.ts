import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, exhaustMap } from 'rxjs/operators';

import { HierarchyActions } from '../actions';
import { HierarchyService } from '../services';

@Injectable()
export class HierarchyEffect {

  findNodes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HierarchyActions.findNodes),
      map(action => action),
      exhaustMap(() =>
        this.hierarchyService.findNodes().pipe(
          map((nodes) => HierarchyActions.findNodesSuccess({ nodes })),
          catchError(error => of(HierarchyActions.findNodesFailure({ error })))
        )
      )
    )
  );
  updateNode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HierarchyActions.updateNode),
      map(action => action.updatedNode),
      exhaustMap((updatedNode: object) =>
        this.hierarchyService.updateNode(updatedNode).pipe(
          map((nodes) => HierarchyActions.updateNodeSuccess({ nodes })),
          catchError(error => of(HierarchyActions.updateNodeFailure({ error })))
        )
      )
    )
  );
  deleteNode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HierarchyActions.deleteNode),
      map(action => action._id),
      exhaustMap((_id: string) =>
        this.hierarchyService.deleteNode(_id).pipe(
          map((deletedNode) => HierarchyActions.deleteNodeSuccess({ deletedNode })),
          catchError(error => of(HierarchyActions.deleteNodeFailure({ error })))
        )
      )
    )
  );

  findNodeTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HierarchyActions.findNodeTypes),
      map(action => action._id),
      exhaustMap((_id: string) =>
        this.hierarchyService.findNodeTypes(_id).pipe(
          map((nodeType) => HierarchyActions.findNodeTypesSuccess({ nodeType })),
          catchError(error => of(HierarchyActions.findNodeTypesFailure({ error })))
        )
      )
    )
  );
  cloneNode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HierarchyActions.cloneNode),
      map(action => action.clonedNode),
      exhaustMap((clonedNode: object) =>
        this.hierarchyService.cloneNode(clonedNode).pipe(
          map((nodes) => HierarchyActions.cloneNodeSuccess({ nodes })),
          catchError(error => of(HierarchyActions.cloneNodeFailure({ error })))
        )
      )
    )
  );
  findSubactionStreams$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HierarchyActions.findSubactionStreams),
      map(action => action._id),
      exhaustMap((_id: string) =>
        this.hierarchyService.findSubactionStreams(_id).pipe(
          map((streams) => HierarchyActions.findSubactionStreamsSuccess({ streams })),
          catchError(error => of(HierarchyActions.findSubactionStreamsFailure({ error })))
        )
      )
    )
  );
  updateStream$ = createEffect(() =>
    this.actions$.pipe(
      ofType(HierarchyActions.updateStreams),
      map(action => action.updatedStreams),
      exhaustMap((updatedStreams: object) =>
        this.hierarchyService.updateStreams(updatedStreams).pipe(
          map((streams) => HierarchyActions.updateStreamsSuccess({ streams })),
          catchError(error => of(HierarchyActions.updateStreamsFailure({ error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private hierarchyService: HierarchyService,
    // private notify: NotificationService
  ) { }
}
