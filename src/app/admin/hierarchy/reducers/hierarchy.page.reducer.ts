import { createReducer, on } from '@ngrx/store';
import { HierarchyActions } from '../actions';

export interface State {
  pending: boolean;
}

export const initialState: State = {
  pending: false
};

export const reducer = createReducer(
  initialState,
  on(
    HierarchyActions.findNodes,
    HierarchyActions.findSubactionStreams,
    (state) => ({ ...state, pending: true })
  ),
  on(
    HierarchyActions.findNodesSuccess,
    HierarchyActions.findNodesFailure,
    HierarchyActions.findSubactionStreamsSuccess,
    (state) => ({ ...state, pending: false })
  )
);
