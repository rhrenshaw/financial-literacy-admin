import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { HierarchyModel } from '../models';
import { HierarchyActions } from '../actions';

export interface State extends EntityState<HierarchyModel> {}

export const adapter: EntityAdapter<HierarchyModel> = createEntityAdapter<HierarchyModel>({
  selectId: (node: HierarchyModel) => node._id
});

export const initialState: State = adapter.getInitialState({});

export const reducer = createReducer(
  initialState,

  on(
    HierarchyActions.findNodes,
    (state) => ({ ...state, pending: true })
  ),
  on(
    HierarchyActions.findNodesSuccess,
    (state, { nodes }) => {
      return adapter.addMany(nodes, state);
    }
  ),

  on(
    HierarchyActions.updateNodeSuccess,
    (state, { nodes }) => {
      return adapter.upsertOne(nodes[0], state);
    }
  ),

  on(
    HierarchyActions.deleteNodeSuccess,
    (state, { deletedNode }) => {
      return adapter.updateOne({ id: deletedNode.toString(), changes: {status: 0} }, state);
    }
  ),

  on(
    HierarchyActions.cloneNodeSuccess,
    (state, { nodes }) => {
      return adapter.upsertMany(nodes, state);
    }
  )
);
