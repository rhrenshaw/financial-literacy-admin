import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { NodeTypeModel } from '../models';
import { HierarchyActions } from '../actions';

export interface State extends EntityState<NodeTypeModel> {}

export const adapter: EntityAdapter<NodeTypeModel> = createEntityAdapter<NodeTypeModel>({
  selectId: (nodeType: NodeTypeModel) => nodeType._id
});

export const initialState: State = adapter.getInitialState({});

export const reducer = createReducer(
  initialState,

  on(
    HierarchyActions.findNodeTypesSuccess,
    (state, { nodeType }) => {
      return adapter.addAll(nodeType, state);
    }
  )
);
