import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { SubactionStreamsModel } from '../models';
import { HierarchyActions } from '../actions';

export interface State extends EntityState<SubactionStreamsModel> {}

export const adapter: EntityAdapter<SubactionStreamsModel> = createEntityAdapter<SubactionStreamsModel>({
  selectId: (subactions: SubactionStreamsModel) => subactions._id
});

export const initialState: State = adapter.getInitialState({});

export const reducer = createReducer(
  initialState,

  on(
    HierarchyActions.clearSubactionStreams,
    (state, {}) => {
      return adapter.removeAll(state);
    }
  ),

  on(
    HierarchyActions.findSubactionStreamsSuccess,
    (state, { streams }) => {
      return adapter.addAll(streams, state);
    }
  ),

  on(
    HierarchyActions.updateStreamsSuccess,
    (state, { streams }) => {
      return adapter.upsertOne(streams[0], state);
    }
  ),
);
