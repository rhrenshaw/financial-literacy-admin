import { createFeatureSelector, Action, combineReducers } from '@ngrx/store';

import { featureKey } from '../hierarchy.config';
import * as fromHierarchy from './hierarchy.reducer';
import * as fromHierarchyPage from './hierarchy.page.reducer';
import * as fromNodeType from './nodetype.reducer';
import * as fromSubactionStreams from './subactionStreams.reducer';

export interface HierarchyState {
  hierarchyPage: fromHierarchyPage.State;
  hierarchy: fromHierarchy.State;
  nodeType: fromNodeType.State;
  subActionStreams: fromSubactionStreams.State;
}

export function reducer(state: HierarchyState | undefined, action: Action) {
  return combineReducers({
    hierarchyPage: fromHierarchyPage.reducer,
    hierarchy: fromHierarchy.reducer,
    nodeType: fromNodeType.reducer,
    subActionStreams: fromSubactionStreams.reducer
  })(state, action);
}

export const selectHierarchyState = createFeatureSelector<HierarchyState>(featureKey);
