import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HierarchyPageComponent } from './containers';

const routes: Routes = [
  {
    path: '',
    component: HierarchyPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HierarchyRoutingModule { }
