export interface NodeTypeModel {
  _id: string;
  attributes: [{
    type: string;
    label: string;
  }];

  // find(param: (elem: object) => boolean): any;
  find(param: (elem: any) => boolean): any;
}
