export interface Entitites2 {
  _id: string;
  value: string;
}
export interface SubactionStreamsModel {
  metadata: Array<Entitites2>;
  author: string;
  status: number;
  _id: string;
  parentId: string;
  text: string;
}
