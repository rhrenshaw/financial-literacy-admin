export interface Entitites {
  _id: string;
  value: string;
}
export interface HierarchyModel {
  metadata: Array<Entitites>;
  author: string;
  status: number;
  _id: string;
  parentId: string;
  text: string;
}
export interface HierarchyCloneModel {
  parentId: string;
  author: string;
  order: number;
  nodes: Array<any>;
}
