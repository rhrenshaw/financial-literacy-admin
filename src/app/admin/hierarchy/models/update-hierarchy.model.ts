export interface HierarchyUpdateModel {
  _id: string;
  type: string;
  order: number;
  parentId: string;
  entities: Array<{ _id: string, value: string, status: number }>;
  author: string;
}
