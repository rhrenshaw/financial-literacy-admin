export interface StreamsUpdateModel {
  _id: string;
  type: string;
  parentId: string;
  entities: Array<{ _id: string, value: string, status: number }>;
  author: string;
}
