import { TreeModule } from 'angular-tree-component';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule } from '@ngx-translate/core';

import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';

import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { DialogModule } from '@app/shared/dialog';
import { NotificationModule } from '@app/shared/notification';
import { ActionSelectionDialogComponent } from './components/action-selection/action-selection.component';
import { HierarchyRoutingModule } from './hierarchy-routing.module';
import { featureKey } from './hierarchy.config';
import { reducer } from './reducers';
import { EFFECTS } from './effects';
import { SERVICES } from './services';
import { CONTAINERS } from './containers';
import { COMPONENTS } from './components';
import { NodeFormComponent } from './components';
import { QuillModule } from 'ngx-quill';
import { StreamsFormComponent } from './components/streams-form/streams-form.component';

const MAT_MODULES = [
  MatTableModule,
  MatButtonModule,
  MatIconModule,
  MatCheckboxModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatMenuModule,
  MatInputModule,
  MatFormFieldModule,
  MatTabsModule,
  MatRadioModule
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forFeature(featureKey, reducer),
    EffectsModule.forFeature(EFFECTS),
    TranslateModule.forChild(),
    PerfectScrollbarModule,
    MAT_MODULES,
    FlexLayoutModule,
    DialogModule,
    NotificationModule,
    HierarchyRoutingModule,
    TreeModule.forRoot(),
    FormsModule,
    QuillModule.forRoot()
  ],
  declarations: [
    CONTAINERS,
    COMPONENTS,
    NodeFormComponent,
    StreamsFormComponent
  ],
  entryComponents: [
    ActionSelectionDialogComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [SERVICES]
})
export class HierarchyModule {
}
