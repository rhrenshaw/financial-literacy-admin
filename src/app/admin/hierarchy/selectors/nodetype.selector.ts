import { createSelector } from '@ngrx/store';

import { selectHierarchyState } from '../reducers';
import * as fromNodeType from '../reducers/nodetype.reducer';

export const selectNodeTypeEntitiesState = createSelector(
  selectHierarchyState,
  state => state.nodeType
);

export const {
  selectIds: selectNodeTypeIds,
  selectEntities: selectNodeTypeEntities,
  selectAll: selectAllNodes,
  selectTotal: selectTotalNodeTypes,
} = fromNodeType.adapter.getSelectors(selectNodeTypeEntitiesState);

export const selectNodeTypeById = (id: string) => createSelector(
  selectNodeTypeEntities,
  (nodetypeEntities) => nodetypeEntities[id]
);
