import { createSelector } from '@ngrx/store';

import { selectHierarchyState } from '../reducers';

export const selectHierarchyPageState = createSelector(
  selectHierarchyState,
  state => state.hierarchyPage
);

export const selectHierarchyPagePending = createSelector(
  selectHierarchyPageState,
  (hierarchyPageState) => hierarchyPageState.pending
);
