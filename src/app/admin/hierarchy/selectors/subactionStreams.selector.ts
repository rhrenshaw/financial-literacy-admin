import { createSelector } from '@ngrx/store';

import { selectHierarchyState } from '../reducers';
import * as fromSubactionStreams from '../reducers/subactionStreams.reducer';

export const selectSubactionStreamsEntitiesState = createSelector(
  selectHierarchyState,
  state => state.subActionStreams
);

export const {
  selectAll: selectAllNodes
} = fromSubactionStreams.adapter.getSelectors(selectSubactionStreamsEntitiesState);
