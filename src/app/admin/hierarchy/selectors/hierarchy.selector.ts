import { createSelector } from '@ngrx/store';

import { selectHierarchyState } from '../reducers';
import * as fromHierarchy from '../reducers/hierarchy.reducer';

export const selectHierarchyEntitiesState = createSelector(
  selectHierarchyState,
  state => state.hierarchy
);

export const {
  selectIds: selectHierarchyIds,
  selectEntities: selectHierarchyEntities,
  selectAll: selectAllNodes,
  selectTotal: selectTotalHierarchys,
} = fromHierarchy.adapter.getSelectors(selectHierarchyEntitiesState);

// export const selectHierarchyById = createSelector(
//   selectHierarchyEntities,
//   hierarchyEntities => (id: string) => hierarchyEntities[id]
// );
