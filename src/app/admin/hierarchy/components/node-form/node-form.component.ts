import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild  } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NodeTypeModel } from '../../models';
import { QuillEditorComponent } from 'ngx-quill';

@Component({
  selector: 'app-node-form',
  templateUrl: './node-form.component.html',
  styleUrls: ['./node-form.component.scss']
})
export class NodeFormComponent implements OnInit, OnChanges {
  @Input() nodeData: any;
  @Input() childNode: any;
  @Input() nodeType: NodeTypeModel;

  @Output() submitForm: EventEmitter<FormGroup> = new EventEmitter();

  group: [];
  myFormGroup: FormGroup;
  selectedNodeType: any;
  selectedNodeTypeAttributes: any;
  private _formLabel: any;
  @ViewChild('editor', {
    static: true
  }) editor: QuillEditorComponent;

  constructor(
    // @ts-ignore
    private domSanitizer: DomSanitizer
  ) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.nodeData.currentValue &&
      changes.nodeData.currentValue.metadata) {
      for (const propName in changes) {
        if (changes.hasOwnProperty(propName)) {
          if (propName === 'nodeData') {
            if (this.nodeData && this.nodeData.metadata) {
              // @ts-ignore
              this.selectedNodeTypeName = this.nodeType.find(elem => {
                return elem._id === this.nodeData.metadata.type._id;
              }).name;
            }
            this.buildForm();
          }
        }
      }
    }
  }

  buildForm() {
    const group = {};

    if (this.nodeType && this.nodeData && this.nodeData.metadata) {
      this.selectedNodeType = this.nodeData.metadata.type._id;
      // @ts-ignore
      this.selectedNodeTypeAttributes = this.nodeType.find(elem => elem._id === this.selectedNodeType).attributes;
      this.selectedNodeTypeAttributes.forEach((inputTemplate, index) => {
        if (this.nodeData.metadata && this.nodeData.metadata.entities[index]) {
          group[inputTemplate.label] = new FormControl(this.nodeData.metadata.entities[index].value);
          if (inputTemplate.type === 'textBoxSlug') {
            group[inputTemplate.label].disable();
          }
          if (inputTemplate.label === 'hierarchy.title') {
            group[inputTemplate.label].valueChanges.subscribe(val => {
              group['hierarchy.slug'].setValue(val.replace(/\s+/g, '-').toLowerCase());
            });
          }
          this.buildValidators(inputTemplate, group[inputTemplate.label]);
        } else {
          group[inputTemplate.label] = new FormControl('');
          if (inputTemplate.label === 'hierarchy.title') {
            group[inputTemplate.label].valueChanges.subscribe(val => {
              group['hierarchy.slug'].setValue(val.replace(/\s+/g, '-').toLowerCase());
            });
          }
        }
        if (this.nodeData.metadata.entities[index]) {
          group[inputTemplate.label].entityId = this.nodeData.metadata.entities[index]._id;
        } else {
          group[inputTemplate.label].entityId = '';
        }
      });
      this.myFormGroup = new FormGroup(group);
    }
  }

  buildValidators(inputTemplate: any, formGroup: FormGroup) {
    if (inputTemplate.fieldValidator) {
      if (inputTemplate.fieldValidator.indexOf('minLength') > -1) {
        const regex = /\d+/g;
        const minLen = parseInt(inputTemplate.fieldValidator.match(regex)[0], 4);
        formGroup.setValidators(Validators.minLength(minLen));
      } else {
        formGroup.setValidators(Validators[inputTemplate.fieldValidator]);
      }
      formGroup.updateValueAndValidity();
    }

    return;
  }

  onUploadChange(evt: any, formLabel: any) {
    this._formLabel = formLabel;
    const file = evt.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (e) => {
        // @ts-ignore
        this.myFormGroup.controls[this._formLabel].setValue('data:image/png;base64,' + btoa(e.target.result));
      };
      reader.readAsBinaryString(file);
    }
  }

  onSubmit() {
    this.submitForm.emit(this.myFormGroup);
  }
}
