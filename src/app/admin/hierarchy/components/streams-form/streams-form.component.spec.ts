import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamsFormComponent } from './streams-form.component';

describe('StreamsFormComponent', () => {
  let component: StreamsFormComponent;
  let fixture: ComponentFixture<StreamsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StreamsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
