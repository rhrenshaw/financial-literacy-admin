import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';

import { StreamsUpdateModel } from '../../models';
import { HierarchyActions } from '../../actions';
import { HierarchyState } from '../../reducers';

@Component({
  selector: 'app-streams-form',
  templateUrl: './streams-form.component.html',
  styleUrls: ['./streams-form.component.scss']
})

export class StreamsFormComponent implements OnInit, OnChanges {
  @Input() subactionStreams: any;
  @Output() submitForm: EventEmitter<FormGroup> = new EventEmitter();

  myFormGroup: FormGroup;
  group: [];

  constructor(private store: Store<HierarchyState>) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.subactionStreams.currentValue && changes.subactionStreams.currentValue.metadata) {
      this.buildForm();
    }
  }

  buildForm() {
    const group = {};
    this.subactionStreams.metadata.entities.forEach((entity) => {
      entity = JSON.parse(JSON.stringify(entity));
      entity.status = 1;
      group[entity._id] = new FormControl(entity.value, Validators.required);
      group[entity._id].entityId = entity._id;
    });
    this.myFormGroup = new FormGroup(group);
  }

  removeItem(control: string, index: number) {
    // if (typeof this.subactionStreams.metadata.entities[index] === integer)
    this.subactionStreams = JSON.parse(JSON.stringify(this.subactionStreams));
    this.subactionStreams.metadata.entities[index].status = 0;
    this.myFormGroup.removeControl(control);
  }

  addItem() {
    const index = this.subactionStreams.metadata.entities.length;
    this.subactionStreams.metadata.entities.push({_id: index.toString(), value: '', status: 1});
    this.myFormGroup.addControl(index, new FormControl('', Validators.required));
  }

  onSubmit() {
    console.log(this.subactionStreams);
    const updatedStreams: StreamsUpdateModel = {
      _id: this.subactionStreams._id,
      entities: [],
      author: this.subactionStreams.metadata.author._id,
      type: this.subactionStreams.metadata.type._id,
      parentId: this.subactionStreams.parentId
    };
    this.subactionStreams.metadata.entities = [];
    for (const key of Object.keys(this.myFormGroup.controls)) {
      this.subactionStreams.metadata.entities.push({
        status: 1,
        _id: key,
        value: this.myFormGroup.controls[key].value
      });
    }
    this.store.dispatch(HierarchyActions.updateStreams({updatedStreams}));
  }
}
