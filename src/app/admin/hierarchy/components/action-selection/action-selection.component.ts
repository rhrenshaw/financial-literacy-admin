import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-action-selection-dialog',
  templateUrl: './action-selection.component.html',
  styleUrls: ['./action-selection.component.scss']
})

export class ActionSelectionDialogComponent implements OnInit {
  myFormGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ActionSelectionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit() {
    const fieldsCtrls = {};

    for (const field of this.data.actionNodesTypes)  {
      fieldsCtrls[field._id] = new FormControl(field.name || '');
    }
    this.myFormGroup = new FormGroup(fieldsCtrls);
  }

  closeDialog() {
    let selectedType: any = {};

    const selectedTypeId = Object.keys(this.myFormGroup.value).filter(elem => {
      return this.myFormGroup.value[elem] === 'on';
    })[0];
    if (!selectedTypeId) {
      selectedType = this.data.actionNodesTypes[0];
    } else {
      selectedType = this.data.actionNodesTypes.filter(elem => {
        return elem._id === selectedTypeId;
      })[0];
    }
    this.dialogRef.close({event: 'close', _id: selectedType._id, name: selectedType.name});
  }
}
