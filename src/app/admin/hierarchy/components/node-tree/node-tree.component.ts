import { Component, Input, Output, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatDialog } from '@angular/material/dialog';
import { ITreeOptions } from 'angular-tree-component';
import { HierarchyCloneModel, HierarchyUpdateModel } from '../../models';
import { ITreeModel, TreeNode } from 'angular-tree-component/dist/defs/api';
import { HierarchyActions } from '../../actions';
import { Store } from '@ngrx/store';
import { HierarchyState } from '../../reducers';
import { ConfirmDialogComponent } from '@app/shared/dialog';
import { ActionSelectionDialogComponent } from '../action-selection/action-selection.component';

@Component({
  selector: 'app-node-tree',
  templateUrl: './node-tree.component.html',
  styleUrls: ['./node-tree.component.scss']
})

export class NodeTreeComponent implements OnInit {
  @Input() allNodesAsTree: any;
  @Input() allNodesTypes: any;
  @Input() authUser: any;
  @Output() nodeSelected: EventEmitter<number> =   new EventEmitter();
  @ViewChild(MatMenuTrigger, null) public menuTrigger: MatMenuTrigger;

  menuLeft = 0;
  menuTop = 0;
  nodeData: any = [];
  selectedNodeType: string;
  state = localStorage.treeState && JSON.parse(localStorage.treeState);
  options: ITreeOptions = {
    nodeClass: (node) => `${node.data.status === 1 ? '' : 'hiddenNode'}`,
    idField: '_id',
    displayField: 'text',
    childrenField: 'children',
    actionMapping: {
      mouse: {
        contextMenu: (_tree, node, $event) => {
          this.openContextMenu($event, _tree, node.data);
        }
      }
    }
  };
  clipboard: TreeNode;

  constructor(
    private store: Store<HierarchyState>,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
  }

  openContextMenu(event: MouseEvent, tree: ITreeModel, node: TreeNode) {
    event.preventDefault();
    tree.getNodeById(node._id).setActiveAndVisible(false);
    this.menuLeft = event.clientX - 220;
    this.menuTop = event.clientY - 100;
    this.menuTrigger.openMenu();
}

  setState(state: any) {
    localStorage.treeState = JSON.stringify(state);
  }

  nodeActivate(event: any) {
    if (event.node.data.metadata.type.type !== 'locale') {
      this.nodeData = event.node.data;
      if (this.nodeData.metadata.type.multi) {
        this.store.dispatch(HierarchyActions.findSubactionStreams({_id: this.nodeData._id }));
      } else {
        this.store.dispatch((HierarchyActions.clearSubactionStreams()));
      }
    } else {
      this.nodeData = null;
      this.selectedNodeType = '';
    }
    this.nodeSelected.emit(this.nodeData);
  }

  nodeDeactivate() {
    this.nodeData = [];
  }

  addNode(tree: any, nodesTypes: any) {
    const selectedNode = tree.treeModel.getActiveNode().data;
    const newNodeType = nodesTypes.find(elem => {
      return elem.parentId === selectedNode.metadata.type._id;
    });
    const updatedNode: HierarchyUpdateModel = {
      _id: '',
      type: newNodeType._id,
      parentId: selectedNode._id,
      entities: [],
      order: selectedNode.children.length,
      author: this.authUser.id
    };
    for (let i = 0; i < newNodeType.attributes.length; i++) {
      updatedNode.entities[i] = {_id: '', value: '', status: 1};
    }
    if (newNodeType.type === 'action') {
      const actionNodesTypes = nodesTypes.filter(elem => {
        return elem.type === 'action';
      });
      this.dialog.open(ActionSelectionDialogComponent, {
        data: {
          title: 'Action type selection',
          message: 'You must select an Action type.  This cannot be changed afterwards.',
          actionNodesTypes
        }
      }).afterClosed()
        .subscribe(result => {
          if (result) {
            updatedNode.type = result._id;
            updatedNode.entities[0].value = 'New ' + result.name;
            updatedNode.entities[1].value = 'New ' + result.name;
            this.store.dispatch(HierarchyActions.updateNode({updatedNode}));
          }
        });
    } else {
      updatedNode.entities[0].value = 'New ' + newNodeType.name;
      updatedNode.entities[1].value = 'New ' + newNodeType.name;
      this.store.dispatch(HierarchyActions.updateNode({updatedNode}));
    }
  }

  deleteNode(tree: any) {
    const selectedNode = tree.treeModel.getActiveNode().data;
    this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm',
        message: 'Are you sure you want to delete this node and all its descendants?'
      }
    }).afterClosed()
      .subscribe(confirm => {
        if (confirm) {
          this.store.dispatch(HierarchyActions.deleteNode(selectedNode));
        }
      });
  }

  copyNode(tree: any) {
    if (tree) {
      this.clipboard = tree.treeModel.getActiveNode();
    }
  }

  isNodeCopyable(tree: any) {
    if (!tree) {
      return;
    }
    const selectedNode = tree.treeModel.getActiveNode();
    if (selectedNode && selectedNode.level > 1 && selectedNode.level < 6) {
      return true;
    }
  }

  isNodeDeleteable(tree: any) {
    if (!tree) {
      return;
    }
    const selectedNode = tree.treeModel.getActiveNode();
    if (selectedNode && selectedNode.level > 0) {
        return true;
    }
  }

  isNodeAddable(tree: any) {
    if (!tree) {
      return false;
    }

    return true;
  }

  pasteNode(tree: any) {
    if (!tree) {
      return;
    }
    const nodeObject: HierarchyCloneModel = {parentId: '', author: '', order: 0, nodes: []};
    function* processData(clipboard: any) {
      if (!clipboard) {
        return;
      }

      for (let i = 0; i < clipboard.length; i++) {
        const val = clipboard[i];
        yield val;

        if (val.children) {
          yield* processData(val.children);
        }
      }
    }

    const it = processData([this.clipboard.data]);
    let res = it.next();
    while (!res.done) {
      nodeObject.nodes.push(res.value._id);
      res = it.next();
    }
    nodeObject.parentId = tree.treeModel.getActiveNode().data._id;
    nodeObject.author = this.authUser.id;
    nodeObject.order = tree.treeModel.getActiveNode().data.children.length;

    this.store.dispatch(HierarchyActions.cloneNode({clonedNode: nodeObject}));
  }

  isNodePastable(tree: any) {
    if (!tree) {
      return;
    }
    const selectedNode: TreeNode = tree.treeModel.getActiveNode();

    return (selectedNode.level === this.clipboard.level - 1 && selectedNode.level < 5);
  }

  // createSubaction(tree: any) {
  //   const selectedNode = tree.treeModel.getActiveNode().data;
  //   this.dialog.open(ConfirmDialogComponent, {
  //     data: {
  //       title: 'Confirm',
  //       message: 'Are you sure you want to delete this node and all its descendants?'
  //     }
  //   }).afterClosed()
  //     .subscribe(confirm => {
  //       if (confirm) {
  //         // this.store.dispatch(HierarchyActions.addSubaction(selectedNode));
  //       }
  //     });
  // }
}
