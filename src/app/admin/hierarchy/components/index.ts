import { NodeMaintenanceComponent } from './node-maintenance/node-maintenance.component';
import { StreamsFormComponent } from './streams-form/streams-form.component';
import { NodeFormComponent } from './node-form/node-form.component';
import { NodeTreeComponent } from './node-tree/node-tree.component';
import { ActionSelectionDialogComponent } from './action-selection/action-selection.component';

export const COMPONENTS = [
  NodeMaintenanceComponent,
  NodeFormComponent,
  StreamsFormComponent,
  NodeTreeComponent,
  ActionSelectionDialogComponent
];

export {
  NodeMaintenanceComponent,
  NodeFormComponent,
  StreamsFormComponent,
  NodeTreeComponent,
  ActionSelectionDialogComponent
};
