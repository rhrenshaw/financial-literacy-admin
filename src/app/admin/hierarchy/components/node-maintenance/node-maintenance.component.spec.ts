import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeMaintenanceComponent } from './node-maintenance.component';

describe('NodeFormComponent', () => {
  let component: NodeMaintenanceComponent;
  let fixture: ComponentFixture<NodeMaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeMaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
