import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Store } from '@ngrx/store';

import { HierarchyModel, HierarchyUpdateModel, NodeTypeModel } from '../../models';
import { HierarchyActions } from '../../actions';
import { HierarchyState } from '../../reducers';
import { UserProfile } from '@app/auth';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-node-maintenance',
  templateUrl: './node-maintenance.component.html',
  styleUrls: ['./node-maintenance.component.scss']
})

export class NodeMaintenanceComponent implements OnInit {
  @Input() nodeData: any;
  @Input() subactionStreams: any;
  @Input() authUser: UserProfile;
  @Input() nodeType: NodeTypeModel;
  @Input() submitForm: FormGroup;

  nodes$: Observable<HierarchyModel>;
  group: [];
  myFormGroup: FormGroup;
  selectedNodeType: any;
  selectedNodeTypeName: any;
  selectedNodeTypeAttributes: any;

  constructor(
    private store: Store<HierarchyState>,
    // @ts-ignore
    private domSanitizer: DomSanitizer
  ) {
  }

  ngOnInit() {
  }

  onSubmitForm(myFormGroup: FormGroup) {
    const updatedNode: HierarchyUpdateModel = {
      _id: this.nodeData._id,
      type: this.nodeData.metadata.type,
      order: this.nodeData.metadata.order,
      parentId: this.nodeData.parentId,
      entities: [],
      author: this.authUser.id
    };

    let index = 0;
    for (const key of Object.keys(myFormGroup.controls)) {
      updatedNode.entities[index] = {value: myFormGroup.controls[key].value, _id:  myFormGroup.controls[key]['entityId'], status: 1 };
      index++;
    }

    this.store.dispatch(HierarchyActions.updateNode({updatedNode}));
  }
}
