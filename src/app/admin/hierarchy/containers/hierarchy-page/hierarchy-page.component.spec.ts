import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HierarchyPageComponent } from './hierarchy-page.component';

describe('hierarchyPageComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HierarchyPageComponent]
    })
      .compileComponents();
  }));

  function setup() {
    const fixture: ComponentFixture<HierarchyPageComponent> = TestBed.createComponent(HierarchyPageComponent);
    const component: HierarchyPageComponent = fixture.componentInstance;
    fixture.detectChanges();

    return { fixture, component };
  }

  it('should create', () => {
    const { component } = setup();

    expect(component).toBeTruthy();
  });
});
