import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { Store } from '@ngrx/store';
import { select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { HierarchyModel } from '../../models';
import { HierarchyState } from '../../reducers';
import { HierarchyActions } from '../../actions';
import { HierarchySelectors, HierarchyPageSelectors } from '../../selectors';

import { AuthState } from '../../../../auth/reducers';
import { AuthSelectors } from '../../../../auth/selectors';
import { UserProfile } from '@app/auth';

import { SubactionStreamsModel } from '../../models';
import { SubactionStreamSelectors } from '../../selectors';

import { NodeTypeModel } from '../../models';
import { NodeTypeSelectors } from '../../selectors';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-hierarchy-page',
  templateUrl: './hierarchy-page.component.html',
  styleUrls: ['./hierarchy-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class HierarchyPageComponent implements OnInit {
  pending$: Observable<boolean>;
  notEmpty$: Observable<boolean>;
  nodes$: Observable<Array<HierarchyModel>>;
  subactionStreams$: Observable<Array<SubactionStreamsModel>>;
  nodeType$: Observable<Array<NodeTypeModel>>;
  authUser$: Observable<UserProfile>;
  nodeData: any = [];
  allNodesAsTree: any = [];

  constructor(
    private store: Store<HierarchyState>,
    private authStore: Store<AuthState>,
  ) {
    this.nodes$ = this.store.pipe(select(HierarchySelectors.selectAllNodes));
    this.subactionStreams$ = this.store.pipe(select(SubactionStreamSelectors.selectAllNodes));
    this.nodeType$ = this.store.pipe(select(NodeTypeSelectors.selectAllNodes));
    this.authUser$ = this.authStore.pipe(select(AuthSelectors.selectLoggedInUser));
    this.pending$ = this.store.pipe(select(HierarchyPageSelectors.selectHierarchyPagePending));
    this.notEmpty$ = this.nodes$.pipe(map(nodes => !!nodes && nodes.length > 0));
    this.store.dispatch(HierarchyActions.findNodes({ query: {} }));
    this.store.dispatch(HierarchyActions.findNodeTypes({_id: ''}));
  }

  ngOnInit() {
    this.nodes$.subscribe(nodes => {
      this.allNodesAsTree = this.unflatten(nodes);
    });
  }

  unflatten(arr: any) {
    const tree = [];
    let mappedArr = {};
    let arrElem = {
      _id: 0
    };
    let mappedElem: { parentId: any, metadata: any, order: number };

    // First map the nodes of the array to an object -> create a hash table.
    for (let i = 0, len = arr.length; i < len; i++) {
      arrElem = arr[i];
      mappedArr[arrElem._id] = arrElem;
      mappedArr = JSON.parse(JSON.stringify(mappedArr));
      mappedArr[arrElem._id].children = [];
      if (mappedArr[arrElem._id].metadata.entities) {
        if (mappedArr[arrElem._id].metadata.entities[1]) {
          mappedArr[arrElem._id].text = mappedArr[arrElem._id].metadata.entities[1].value;
        } else {
          mappedArr[arrElem._id].text = mappedArr[arrElem._id].metadata.entities[0].value;
        }
      } else {
        mappedArr[arrElem._id].text = 'Subaction';
      }
      mappedArr[arrElem._id].text += ' (' + arrElem._id.toString().substring(19, 24) + ')';
    }
    for (const _id in mappedArr) {
      if (mappedArr.hasOwnProperty(_id)) {
        mappedElem = mappedArr[_id];
        // If the element is not at the root level, add it to its parent array of children.
        if (mappedElem.parentId) {
          mappedArr[mappedElem.parentId].children[mappedElem.metadata.order] = mappedElem;
        } else {
          tree.push(mappedElem);
        }
      }
    }

    return tree;
  }

  nodeSelected(event: any = []) {
    this.nodeData = event;
  }
}
