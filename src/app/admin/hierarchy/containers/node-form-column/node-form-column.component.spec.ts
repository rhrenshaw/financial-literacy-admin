import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeFormColumnComponent } from './node-maintenance-column.component';

describe('NodeFormColumnComponent', () => {
  let component: NodeFormColumnComponent;
  let fixture: ComponentFixture<NodeFormColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeFormColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeFormColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
