import { HierarchyPageComponent } from './hierarchy-page/hierarchy-page.component';

export const CONTAINERS = [
  HierarchyPageComponent
];

export {
  HierarchyPageComponent
};
