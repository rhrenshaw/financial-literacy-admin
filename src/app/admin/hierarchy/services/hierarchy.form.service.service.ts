import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
// import { HierarchyModel } from '../models/hierarchy.model';
@Injectable({
  providedIn: 'root'
})
export class HierarchyFormService {
  myFormGroup: FormGroup;

  constructor() {
  }

  createForm(nodeType: any, nodeData: any) {
    const group = {};
    const selectedNodeType = nodeData.metadata.type._id;
    const selectedNodeTypeAttributes = nodeType.find(elem => elem._id === selectedNodeType).attributes;
    selectedNodeTypeAttributes.forEach((inputTemplate, index) => {
        if (nodeData.metadata && nodeData.metadata.entities[index]) {
          group[inputTemplate.label] = new FormControl(nodeData.metadata.entities[index].value);
          if (inputTemplate.type === 'textBoxSlug') {
            group[inputTemplate.label].disable();
          }
          if (inputTemplate.label === 'hierarchy.title') {
            group[inputTemplate.label].valueChanges.subscribe(val => {
              group['hierarchy.slug'].setValue(val.replace(/\s+/g, '-').toLowerCase());
            });
          }
          this.buildValidators(inputTemplate, group[inputTemplate.label]);
        } else {
          group[inputTemplate.label] = new FormControl('');
          if (inputTemplate.label === 'hierarchy.title') {
            group[inputTemplate.label].valueChanges.subscribe(val => {
              group['hierarchy.slug'].setValue(val.replace(/\s+/g, '-').toLowerCase());
            });
          }
        }
        if (nodeData.metadata.entities[index]) {
          group[inputTemplate.label].entityId = nodeData.metadata.entities[index]._id;
        } else {
          group[inputTemplate.label].entityId = '';
        }
      });

    return new FormGroup(group);
    }

  buildValidators(inputTemplate: any, formGroup: FormGroup) {
    if (inputTemplate.fieldValidator) {
      if (inputTemplate.fieldValidator.indexOf('minLength') > -1) {
        const regex = /\d+/g;
        const minLen = parseInt(inputTemplate.fieldValidator.match(regex)[0], 4);
        formGroup.setValidators(Validators.minLength(minLen));
      } else {
        formGroup.setValidators(Validators[inputTemplate.fieldValidator]);
      }
      formGroup.updateValueAndValidity();
    }

    return;
  }
}
