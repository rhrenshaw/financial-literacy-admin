import { HierarchyService } from './hierarchy.service';

export const SERVICES = [
  HierarchyService
];

export {
  HierarchyService
};
