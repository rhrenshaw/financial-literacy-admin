import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HierarchyService {
  constructor(private http: HttpClient) { }

  public findNodes(): Observable<any> {
    return this.http.get('api/hierarchy/getnodes', {});
  }

  public findNodeTypes(_id: string): Observable<any> {
    return this.http.post('api/hierarchy/gettype', {id: _id});
  }

  public updateNode(updatedNode: object): Observable<any> {
    return this.http.post('api/hierarchy/updatenode', updatedNode);
  }

  public deleteNode(_id: string): Observable<any> {
    return this.http.post('api/hierarchy/deletenode', {id: _id});
  }

  public cloneNode(clonedNode: object): Observable<any> {
    return this.http.post('api/hierarchy/clonenode', clonedNode);
  }

  public findSubactionStreams(_id: string): Observable<any> {
    return this.http.post('api/hierarchy/getstreams', {parentId: _id});
  }

  public updateStreams(updatedStreams: object): Observable<any> {
    return this.http.post('api/hierarchy/updatestreams', updatedStreams);
  }
}
