import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { HierarchyService } from './hierarchy.service';
describe('HierarchyService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [ HierarchyService ]
    });
  });

  function setup() {
    const service: HierarchyService = TestBed.inject(HierarchyService);
    const backend: HttpTestingController = TestBed.inject(HttpTestingController);

    return { service, backend };
  }

  it('should be created', () => {
    const { service } = setup();
    expect(service).toBeTruthy();
  });

});
