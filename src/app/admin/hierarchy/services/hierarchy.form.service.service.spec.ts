import { TestBed } from '@angular/core/testing';

import { HierarchyFormServiceService } from './hierarchy.form.service.service';

describe('HierarchyFormServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HierarchyFormServiceService = TestBed.inject(HierarchyFormServiceService);
    expect(service).toBeTruthy();
  });
});
