import { createAction, props } from '@ngrx/store';

import { HttpError } from '@app/core/exception';
import { HierarchyModel, HierarchyUpdateModel, HierarchyDeleteModel, HierarchyCloneModel, StreamsUpdateModel } from '../models';
import { NodeTypeModel } from '../models';
import { SubactionStreamsModel } from '../models';

export const findNodes = createAction(
  '[Hierarchy] Find nodes',
  props<{ query: any }>()
);
export const findNodesSuccess = createAction(
  '[Hierarchy] Find nodes by id success',
  props<{ nodes: Array<HierarchyModel> }>()
);
export const findNodesFailure = createAction(
  '[Hierarchy] Find node by id failure',
  props<{ error: HttpError }>()
);

export const findNodeTypes = createAction(
  '[NodeType] Find node by id',
  props<{ _id: string }>()
);
export const findNodeTypesSuccess = createAction(
  '[NodeType] Find nodes by id success',
  props<{ nodeType: Array<NodeTypeModel> }>()
);
export const findNodeTypesFailure = createAction(
  '[NodeType] Find node by id failure',
  props<{ error: HttpError }>()
);

export const updateNode = createAction(
  '[Hierarchy] Update node',
  props<{ updatedNode: HierarchyUpdateModel }>()
);
export const updateNodeSuccess = createAction(
  '[Hierarchy] Update node success',
  props<{ nodes: Array<HierarchyModel> }>()
);
export const updateNodeFailure = createAction(
  '[Hierarchy] Update node failure',
  props<{ error: HttpError }>()
);

export const deleteNode = createAction(
  '[Hierarchy] delete node',
  props<{ _id: string }>()
);
export const deleteNodeSuccess = createAction(
  '[Hierarchy] delete node success',
  props<{ deletedNode: Array<HierarchyDeleteModel> }>()
);
export const deleteNodeFailure = createAction(
  '[Hierarchy] delete node failure',
  props<{ error: HttpError }>()
);

export const cloneNode = createAction(
  '[Hierarchy] Clone node',
  props<{ clonedNode: HierarchyCloneModel }>()
);
export const cloneNodeSuccess = createAction(
  '[Hierarchy] Clone node success',
  props<{ nodes: Array<HierarchyModel> }>()
);
export const cloneNodeFailure = createAction(
  '[Hierarchy] Clone node failure',
  props<{ error: HttpError }>()
);

export const clearSubactionStreams = createAction(
  '[Streams] Clear subaction Streams in repository'
);
export const findSubactionStreams = createAction(
  '[Streams] Find subaction Streams by id',
  props<{ _id: string }>()
);
export const findSubactionStreamsSuccess = createAction(
  '[Streams] Find subaction Streams by id success',
  props<{ streams: Array<SubactionStreamsModel> }>()
);
export const findSubactionStreamsFailure = createAction(
  '[Streams] Find subaction Streams by id failure',
  props<{ error: HttpError }>()
);
export const updateStreams = createAction(
  '[Streams] Update streams',
  props<{ updatedStreams: StreamsUpdateModel }>()
);
export const updateStreamsSuccess = createAction(
  '[Streams] Update streams success',
  props<{ streams: StreamsUpdateModel }>()
);
export const updateStreamsFailure = createAction(
  '[Streams] Update streams failure',
  props<{ error: HttpError }>()
);
