import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'contacts',
        loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule)
      },
      {
        path: 'locales',
        loadChildren: () => import('./locales/locales.module').then(m => m.LocalesModule)
      },
      {
        path: 'hierarchy',
        loadChildren: () => import('./hierarchy/hierarchy.module').then(m => m.HierarchyModule),
        data: {
          // restricted: true // Custom Property
        }
      },
      // {
      //   path: 'chat',
      //   loadChildren: () => import('./chat/chat.module').then(m => m.ChatModule)
      // },
      {
        path: '**',
        redirectTo: ''
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class AdminRoutingModule { }
