const mongoose = require('mongoose');

const UsersSchema = mongoose.Schema({
  first_name: String,
  last_name: String,
  email: String,
  status: { type: Number, default: 1 },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updated: { type: Date, default: Date.now }
});

const LocalesSchema = mongoose.Schema({
  metadata: {
    type: { type: mongoose.Schema.Types.ObjectId, ref: 'node_types' },
    entities: [{ type: mongoose.Schema.Types.ObjectId, ref: 'entities' }],
    order: { type: Number },
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
    updated: { type: Date, default: Date.now }
  },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
  updated: { type: Date, default: Date.now }
});

const NodesSchema = mongoose.Schema({
  metadata: {
    type: { type: mongoose.Schema.Types.ObjectId, ref: 'node_types' },
    entities: [{ type: mongoose.Schema.Types.ObjectId, ref: 'entities' }],
    streams: [{ type: mongoose.Schema.Types.ObjectId, ref: 'entities' }],
    order: { type: Number },
    key: { type: Number },
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
    updated: { type: Date, default: Date.now }
  },
  text: { type: String },
  parentId: { type: mongoose.Schema.Types.ObjectId },
  children: { type: mongoose.Schema.Types.ObjectId, ref: 'nodes' },
  status: { type: Number, default: 1 }
});

const SubactionsSchema = mongoose.Schema({
  metadata: {
    type: { type: mongoose.Schema.Types.ObjectId, ref: 'node_types' },
    stream_entities: [{ entities: [{type: mongoose.Schema.Types.ObjectId, ref: 'entities'}] }],
    order: { type: Number },
    key: { type: Number },
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
    updated: { type: Date, default: Date.now }
  },
  text: { type: String },
  parentId: { type: mongoose.Schema.Types.ObjectId },
  children: { type: mongoose.Schema.Types.ObjectId, ref: 'nodes' },
  status: { type: Number, default: 1 }
});
const SubactionStreamsSchema = mongoose.Schema({
  metadata: {
    type: { type: mongoose.Schema.Types.ObjectId, ref: 'node_types' },
    entities: [{ type: mongoose.Schema.Types.ObjectId, ref: 'entities' }],
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
    updated: { type: Date, default: Date.now }
  },
  parentId: { type: mongoose.Schema.Types.ObjectId },
  children: { type: mongoose.Schema.Types.ObjectId, ref: 'nodes' },
  status: { type: Number, default: 1 }
});

const EntitiesSchema = mongoose.Schema({
  value: String,
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
  status: { type: Number, default: 1 }
});

const NodeTypeSchema = mongoose.Schema({
  type: String,
  name: String,
  parentId:  { type: mongoose.Schema.Types.ObjectId, ref: 'nodeTypes' },
  attributes: Array,
  multi: Boolean,
  status: { type: Number, default: 1 },
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
  updated: { type: Date, default: Date.now }
});

const Users = mongoose.model('users', UsersSchema);
const Locales = mongoose.model('locales', LocalesSchema);
const Nodes = mongoose.model('nodes', NodesSchema);
const Subactions = mongoose.model('subactions',  SubactionsSchema);
const SubactionStreams = mongoose.model('subaction_streams',  SubactionStreamsSchema);
const Entities = mongoose.model('entities', EntitiesSchema);
const NodeType = mongoose.model('node_types', NodeTypeSchema);

module.exports = {
  Users: Users,
  Locales: Locales,
  Nodes: Nodes,
  Subactions: Subactions,
  SubactionStreams: SubactionStreams,
  Entities: Entities,
  NodeType: NodeType
};
