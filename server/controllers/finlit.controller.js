const FinlitModels = require('../models/finlit.model.js');
const appConfig = require('../config/app.config');
const mongoose = require('mongoose');
const fs = require('fs');
const jwt = require('jsonwebtoken');

function checkAuth(token) {
  let publicKEY = fs.readFileSync('./config/public.key', 'utf8');
  if (token) {
    let tokenSplit = token.split(' ')[1];
    return (jwt.verify(tokenSplit, publicKEY));
  }
}

// Authenticate a user login
exports.login = (req, res) => {
  const email = req.body.username;
  const password = req.body.password;
  let client_payload = {};

  FinlitModels.Users.find({email: email, password: password})
    .then(userData => {
      if (userData.length > 0) {
// Payload - for future use
        let payload = {
          data1: 'placeholder'
        };
// Read private and public keys
        var privateKEY = fs.readFileSync('./config/private.key', 'utf8');
        var publicKEY = fs.readFileSync('./config/public.key', 'utf8');
        var i = 'Bilateral Designs';          // Issuer
        var s = 'admin@bilateral.ca';        // Subject
        var a = 'http://bilateral.ca'; // Audience

// Set Sign options
        var signOptions = {
          issuer: i,
          subject: s,
          audience: a,
          expiresIn: "12h",
          algorithm: "RS256"
        };
        var token = jwt.sign(payload, privateKEY, signOptions);
        client_payload =
          {
            first_name: userData[0].first_name,
            last_name: userData[0].last_name,
            username: userData[0].email,
            _id: userData[0]._id,
            accessToken: token,
            expiresIn: "3600",
            tokenType: "Bearer"
          };
      }
      res.send(client_payload);
    }).catch(err => {
    res.status(500).send({
      message: err.message || "Error occurred while validating credentials"
    });
  });
};

//Get all users for maintenance
exports.usersGetAllUsers = (req, res) => {
  // if (checkAuth(req.headers.authorization)) {
    FinlitModels.Users.find({})
      .then(userData => {
        res.send(userData);
      }).catch(err => {
      res.status(500).send({
        message: err.message || "Error occurred while retrieving Users."
      });
    });
  // } else {
  //   res.send('Invalid token');
  // }
};

exports.subactionsGetSubactions = (req, res) => {
  let hierarchy = [];
  FinlitModels.Subactions.find({})
    .populate({path:'metadata.streams.entities', match: { status: 1 }, select: 'value'})
    .populate({path:'metadata.author', select: ['first_name', 'last_name']})
    .populate({path:'metadata.type', select: ['type']})
    .then(nodeData => {
      hierarchy = hierarchy.concat(nodeData);
      res.send(hierarchy);
    }).catch(err => {
    res.status(500).send({
      message: err.message || "Error occurred while retrieving Nodes."
    });
  });
};
// Get specific subaction stream list
exports.subactionsGetStreams = (req, res) => {
  // if (checkAuth(req.headers.authorization)) {
  let parentId = req.body.parentId;
  FinlitModels.SubactionStreams.find({parentId: parentId})
    .populate({path:'metadata.entities', match: { status: 1 }, select: 'value'})
    .populate({path:'metadata.type', match: { status: 1 }, select: 'name'})
    .populate({path:'metadata.author', select: ['first_name', 'last_name']})
    .select('metadata.updated parentId')
    .then(streamData => {
      res.send(streamData);
    }).catch(err => {
    res.status(500).send({
      message: err.message || "Error occurred while retrieving Subaction streams."
    });
  })
  // } else {
  //   res.send('Invalid session');
  // }
};
// Update a Subaction stream and it's entities
exports.subactionsUpdateStreams = (req, res) => {
  // if (checkAuth(req.headers.authorization)) {
  let promiseQueue = [];
  let collection = [];
  let payload = req.body;

  async function updateEntities() {
    payload.entities.forEach(entity => {
      if (mongoose.Types.ObjectId.isValid(entity._id.toString())) {
        promiseQueue.push(updateEntity(entity, payload.author, res))
      } else {
        promiseQueue.push(insertEntity(entity, payload.author, res))
      }
    });
    await Promise.all(promiseQueue)
      .then(function(item) {
        collection.push(item);
      })
      .catch(function(err) {
        res.send(err);
      });
  }

  updateEntities().then(result => {
    let entityArray = [];
    collection[0].forEach(elem => {
      entityArray.push(elem._id.toString());
    });

    if (!payload._id) {
      let newNode = new FinlitModels.SubactionStreams({});
      newNode.save()
        .then(model => {
          saveNewNode(model._id.toString());
        })
    } else {
      saveNewNode(payload._id);
    }

    function saveNewNode(_id) {
      FinlitModels.SubactionStreams.findOneAndUpdate(
        {_id: _id},
        {
          'metadata.type': payload.type,
          'parentId': payload.parentId,
          'status': payload.status,
          'metadata.entities': entityArray,
          'metadata.order': payload.order,
          'metadata.author': payload.author,
          'metadata.updated': new Date()
        })
        .then(result => {
          FinlitModels.SubactionStreams.find({_id: _id})
            .populate({path:'metadata.entities', match: { status: 1 }, select: 'value'})
            .populate({path:'metadata.type', match: { status: 1 }, select: 'name'})
            .populate({path:'metadata.author', select: ['first_name', 'last_name']})
            .select('metadata.updated parentId')
            .then(substreamData => {
              res.send(substreamData);
            }).catch(err => {
            res.status(500).send({
              message: err.message || "Error occurred while saving Node"
            });
          });
        }).catch(err => {
        res.send(err);
      });
    }
  })
  // } else {
  //   res.send('Invalid token');
  // }
};

// Get all Nodes for maintenance
exports.nodesGetNodes = (req, res) => {
  // if (checkAuth(req.headers.authorization)) {
    let hierarchy = [];
    FinlitModels.Locales.find({})
      .populate({path:'metadata.entities', match: { status: 1 }, select: 'value'})
      .populate({path:'metadata.type', select: ['type']})
      .then(localeData => {
        hierarchy = localeData;
        FinlitModels.Nodes.find({})
          .populate({path:'metadata.entities', match: { status: 1 }, select: 'value'})
          .populate({path:'metadata.streams', match: { status: 1 }, select: 'value'})
          .populate({path:'metadata.author', select: ['first_name', 'last_name']})
          .populate({path:'metadata.type', select: ['type', 'multi']})
          .then(nodeData => {
                hierarchy = hierarchy.concat(nodeData);
                res.send(hierarchy);
              }).catch(err => {
              res.status(500).send({
                message: err.message || "Error occurred while retrieving Nodes."
          });
        });
      })
  // } else {
  //   res.send('Invalid session');
  // }
};
// Update a node and it's entities
exports.updateNode = (req, res) => {
  // if (checkAuth(req.headers.authorization)) {
  let promiseQueue = [];
  let collection = [];
  let payload = req.body;

  async function updateEntities() {
    payload.entities.forEach(entity => {
      if (entity._id) {
        promiseQueue.push(updateEntity(entity, payload.author, res))
      } else {
        promiseQueue.push(insertEntity(entity, payload.author, res))
      }
    });
    await Promise.all(promiseQueue)
      .then(function(item) {
        collection.push(item);
      })
      .catch(function(err) {
        res.send(err);
      });
  }

  updateEntities().then(result => {
    let entityArray = [];
    collection[0].forEach(elem => {
      entityArray.push(elem._id.toString());
    });

    if (!payload._id) {
      let newNode = new FinlitModels.Nodes({});
      newNode.save()
        .then(model => {
          saveNewNode(model._id.toString());
        })
    } else {
      saveNewNode(payload._id);
    }

    function saveNewNode(_id) {
      FinlitModels.Nodes.findOneAndUpdate(
        {_id: _id},
        {
          'metadata.type': payload.type,
          'parentId': payload.parentId,
          'metadata.entities': entityArray,
          'metadata.order': payload.order,
          'metadata.author': payload.author,
          'metadata.updated': new Date()
        })
        .then(result => {
          FinlitModels.Nodes.find({_id: _id})
            .populate({path:'metadata.entities', match: { status: 1 }, select: 'value'})
            .populate({path:'metadata.author', select: ['first_name', 'last_name']})
            .populate({path:'metadata.type', select: ['type', 'multi']})
            .then(nodeData => {
              res.send(nodeData);
            }).catch(err => {
            res.status(500).send({
              message: err.message || "Error occurred while saving Node"
            });
          });
        }).catch(err => {
          res.send(err);
      });
    }
  })
  // } else {
  //   res.send('Invalid token');
  // }
};
// Update a node and it's entities
exports.deleteNode = (req, res) => {
  // if (checkAuth(req.headers.authorization)) {
  const selectedNode = req.body.id;
  FinlitModels.Nodes.findOneAndUpdate({_id: selectedNode}, { $set: { status: 0 }})
    .then(nodeData => {
      res.send(nodeData._id);
    }).catch(err => {
    res.status(500).send({
      message: err.message || "Error occurred while retrieving Users."
    });
  });
  // } else {
  //   res.send('Invalid token');
  // }
};
//Get all Nodes for maintenance
exports.nodesGetType = (req, res) => {
  // if (checkAuth(req.headers.authorization)) {
  // let typeId = new mongoose.Types.ObjectId(req.body.id);
    FinlitModels.NodeType.find({})
      .sort({seq: 1})
      .then(nodeTypeData => {
        res.send(nodeTypeData);
      }).catch(err => {
      res.status(500).send({
        message: err.message || "Error occurred while retrieving Nodes"
      });
    });
  // } else {
  //   res.send('Invalid token');
  // }
};
// Clone bode abd a=ll descendants
exports.clonenode = (req, res) => {
// if (checkAuth(req.headers.authorization)) {
  let parentId = req.body.parentId;
  let author = req.body.author;
  let order = req.body.order;
  let idArray = req.body.nodes;
  let promiseQueue = [];
  let entityArray = [];

  async function duplicateEntities(nodesById, author, order) {
    nodesById.forEach((node, nodeIndex) => {
      promiseQueue.push(duplicateEntity(node.metadata.entities, nodeIndex, author, order))
    });
    await Promise.all(promiseQueue)
      .then(function (item) {
        // return item;
      })
      .catch(function (err) {
        res.send(err);
      });
  }
  async function duplicateEntity(nodeEntities, nodeIndex, author, order) {
    return new Promise((resolve, reject) => {
      FinlitModels.Entities.find({_id: {$in: nodeEntities}})
        .then(entityList => {
          // nodesById[nodeIndex].metadata.entities = [];
          entityList.forEach(entity => {
            const newId = mongoose.Types.ObjectId();
            entity._id = newId;
            entity.author = mongoose.Types.ObjectId(author);
            entity.updated = new Date();
            entityArray.push(entity);
          });
          resolve(entityList);
        }).catch(err => {
        res.send(err);
      })
    })
  }

// Get an array of all the nodes, switch _id value and resave them
  function duplicateNodes() {
    FinlitModels.Nodes.find({'_id': {$in: idArray}})
      .then(nodesById => {
        nodesById[0].parentId = parentId;
        for (const nodeById of nodesById) {
          const newId = mongoose.Types.ObjectId();
          for (const element of nodesById) {
            if (element.parentId.toString() === nodeById._id.toString()) {
              element.parentId = mongoose.Types.ObjectId(newId)
            }
          }
          nodeById._id = mongoose.Types.ObjectId(newId);
          nodeById.metadata.author = mongoose.Types.ObjectId(author);
          nodeById.metadata.updated = new Date();
        }

        duplicateEntities(nodesById, author, order)
          .then(result => {
            nodesById[0].metadata.order = order;
            FinlitModels.Nodes.insertMany(nodesById)
              .then(result => {
                FinlitModels.Entities.insertMany(entityArray)
                  .then(result => {
                    exports.nodesGetNodes(req, res);
                  }).catch(err => {
                  res.send(err);
                });
              })
          });
      });
  }

  duplicateNodes();

  // } else {
  //   res.send('Invalid session');
  // }
};

async function updateEntity(entity, author, res) {
  return new Promise((resolve, reject) => {
    if (appConfig.archiveEntities) {
      FinlitModels.Entities.findOneAndUpdate(
        {_id: entity._id},
        {
          author: author,
          status: 0,
          updated: new Date()
        }, {new: true})
        .then(result => {
          let newEntity = new FinlitModels.Entities({value: entity.value, author: author, status: 1, updated: new Date()});
          newEntity.save()
            .then(result => {
              resolve(result);
            }).catch(err => {
            res.send(err);
          })
        }).catch(err => {
        res.send(err);
      })
    } else {
      FinlitModels.Entities.findOneAndUpdate(
        {_id: entity._id},
        {
          value: entity.value,
          author: author,
          status: 1,
          updated: new Date()
        }, {new: true})
        .then(result => {
          resolve(result);
        }).catch(err => {
        res.send(err);
      })
    }
  })
}
async function insertEntity(entity, author, res) {
  let newEntity = new FinlitModels.Entities({value: entity.value, author: author, status: entity.status, updated: new Date()});
  return new Promise((resolve, reject) => {
    newEntity.save()
      .then(result => {
        resolve(result);
      }).catch(err => {
      res.send(err);
    })
  })
}
