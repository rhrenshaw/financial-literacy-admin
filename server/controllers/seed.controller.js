const FinlitModels = require('../models/finlit.model.js');
const mongoose = require('mongoose');

exports.seedDB = (req, res) => {
  const user = new FinlitModels.User;
  let param = {};

  let clearCollections = function() {
    const locales = new FinlitModels.Locale();
    const branch = new FinlitModels.Branch();
    const branch_attribute = new FinlitModels.BranchAttributes();
    const branchValues = new FinlitModels.BranchValues();

    promise = new Promise(function(resolve, reject){
      user.collection.drop().then(() => {
        console.log('User collection cleared');
        resolve();
      }).catch(() =>{
        console.log('User collection not cleared');
        resolve();
      })
        .then(() => {
          locales.collection.drop().then(() => {
            console.log('Locale collection cleared');
            resolve();
          }).catch(() =>{
            console.log('Locale collection not cleared');
            resolve();
          })
      })
        .then(() => {
          branch.collection.drop().then(() => {
            console.log('branch collection cleared');
            resolve();
          }).catch(() =>{
            console.log('branch collection not cleared');
            resolve();
          })
        })
        .then(() => {
          branch_attribute.collection.drop().then(() => {
            console.log('branch_attribute collection cleared');
            resolve();
          }).catch(() =>{
            console.log('branch_attribute collection not cleared');
            resolve();
          })
        })
        .then(() => {
          branchValues.collection.drop().then(() => {
            console.log('branchValues collection cleared');
            resolve();
          }).catch(() =>{
            console.log('branchValues collection not cleared');
            resolve();
          })
        })
    });
    return promise;
  };
  let populateUsers = function() {
    promise = new Promise(function(resolve, reject){
      const user = new FinlitModels.User({
        first_name: "Bob",
        last_name: "Noel"
      });
      user.save().then(userData => {
        console.log('User collection saved');
        resolve({userId: userData._id});
      }).catch(() =>{
        console.log('User collection not saved');
        resolve();
      })
    });
    return promise;
  };
  let populateLocales = function() {
    promise = new Promise(function(resolve, reject){
      let localesArray = [];

      localesArray.push({
        locale_short: "ca-en",
        locale_long: "English",
        locale_translations: {
          "ca-fr": "French",
          "us-en": "US English"
        }
      });
      localesArray.push({
          locale_short: "ca-fr",
          locale_long: "French",
          locale_translations: {
            "ca-en": "Anglais",
            "us-en": "US Anglais"
        }
      });
      localesArray.push({
          locale_short: "us-en",
          locale_long: "US English",
          locale_translations: {
            "ca-en": "English",
            "ca-fr": "French",
        }
      });
      const locales = new FinlitModels.Locale(localesArray);
      locales.collection.insertMany(localesArray).then(localeData => {
        console.log(localeData);
        param.locales = [];
        for(id in localeData.insertedIds) {
          param.locales.push(localeData.insertedIds[id].toString())
        }

        resolve(param);
      }).catch((error) =>{
        console.log('AttributeValue collection not saved:', error);
        resolve();
      });

      // let locales = new FinlitModels.Locale({
      //   locale_short: "ca-en",
      //   locale_long: "English",
      //   locale_translations: {
      //     "ca-en": "English",
      //     "ca-fr": "French"
      //   },
      //   changedBy: mongoose.Types.ObjectId(param.userId)
      // });
      // locales.save().then(localeData => {
      //   let locales = new FinlitModels.Locale({
      //     locale_short: "ca-fr",
      //     locale_long: "French",
      //     locale_translations: {
      //       "ca-en": "Anglais",
      //       "ca-fr": "Francais"
      //     },
      //     changedBy: mongoose.Types.ObjectId(param.userId)
      //   });
      //   param.localeEN = localeData._id;
      //   locales.save().then(localeData => {
      //     console.log('Locale collection saved');
      //     param.localeFR = localeData._id;
      //     resolve(param);
      //   }).catch(() =>{
      //     console.log('Locale collection not saved');
      //     resolve();
      //   })
      // }).catch(() =>{
      //   console.log('Locale collection not saved');
      //   resolve();
      // })
    });
    return promise;
  };
  let populateBranches = function() {
    promise = new Promise(function(resolve, reject){
      const branch = new FinlitModels.Branch({
        parent : mongoose.Types.ObjectId('000000000000000000000000'),
        type: mongoose.Types.ObjectId('5b1a8b8b09fd114104ce8954'),
        seq: 0,
        locales: param.locales,
        changedBy: mongoose.Types.ObjectId(param.userId)
      });
      branch.save().then(branchValueData => {
        console.log('Branch collection saved');
        param.branchValue = branchValueData._id;
        resolve(param);
      }).catch(() =>{
        console.log('Branch collection not saved');
        resolve();
      })
    });
    return promise;
  };
  let populateAttribute = function(param) {
    promise = new Promise(function(resolve, reject){
      const BranchAttribute = new FinlitModels.BranchAttributes({
        type: 'text',
        changedBy: mongoose.Types.ObjectId(param.userId)
      });
      BranchAttribute.save().then(attributeData => {
          param.attributeOne = attributeData._id;
          const attribute = new FinlitModels.BranchAttributes({
            type: 'textArea',
            changedBy: mongoose.Types.ObjectId(param.userId)
          });
        BranchAttribute.save().then(attributeData => {
            console.log('attribute collection #1 saved');
            param.attributeTwo = attributeData._id;
          }).catch(() =>{
            console.log('Branch Attribute collection not saved');
            resolve();
          });
          console.log('Branch Attribute collection #2 saved');
          param.attribute = attributeData._id;
          resolve(param);
        }).catch(() =>{
          console.log('Branch Attribute collection not saved');
          resolve();
        })
    });
    return promise;
  };
  let populateValueOne = function(param) {
    promise = new Promise(function(resolve, reject){
      const branchValues = new FinlitModels.BranchValues({
        value: "Title #1",
        branch: mongoose.Types.ObjectId(param.branchValue),
        locale: param.localeEN,
        seq: 0,
        attribute: param.attributeOne,
        changedBy: mongoose.Types.ObjectId(param.userId)
      });
        branchValues.save().then(branchValueData => {
          console.log('branchValueOne collection saved');
          param.branchValueOne = branchValueData._id;
          resolve(param);
        }).catch(() =>{
          console.log('BranchValues collection not saved');
          resolve();
        })
    });
    return promise;
  };
  let populateValueTwo = function(param) {
    promise = new Promise(function(resolve, reject){
      const branchValues = new FinlitModels.BranchValues({
        value: "Le Title #1",
        branch: mongoose.Types.ObjectId(param.branchValue),
        seq: 0,
        locale: param.localeFR,
        attribute: param.attributeOne,
        changedBy: mongoose.Types.ObjectId(param.userId)
      });
        branchValues.save().then(branchValueData => {
          console.log('branchValueOne collection saved');
          param.branchValueTwo = branchValueData._id;
          resolve(param);
        }).catch(() =>{
          console.log('BranchValues collection not saved');
          resolve();
        })
    });
    return promise;
  };
  let populateValueThree = function(param) {
    promise = new Promise(function(resolve, reject){
      const branchValues = new FinlitModels.BranchValues({
        value: "Value #1",
        branch: mongoose.Types.ObjectId(param.branchValue),
        locale: param.localeEN,
        seq: 1,
        attribute: param.attributeTwo,
        changedBy: mongoose.Types.ObjectId(param.userId)
      });
        branchValues.save().then(branchValueData => {
          console.log('branchValueOne collection saved');
          param.branchValueThree = branchValueData._id;
          resolve(param);
        }).catch(() =>{
          console.log('BranchValues collection not saved');
          resolve();
        })
    });
    return promise;
  };
  let populateValueFour = function(param) {
    promise = new Promise(function(resolve, reject){
      const branchValues = new FinlitModels.BranchValues({
        value: "Le Value #1",
        branch: mongoose.Types.ObjectId(param.branchValue),
        locale: param.localeFR,
        seq: 1,
        attribute: param.attributeTwo,
        changedBy: mongoose.Types.ObjectId(param.userId)
      });
        branchValues.save().then(branchValueData => {
          console.log('branchValueOne collection saved');
          param.branchValueFour = branchValueData._id;
          resolve(param);
        }).catch(() =>{
          console.log('BranchValues collection not saved');
          resolve();
        })
    });
    return promise;
  };

  clearCollections()
    .then(populateUsers)
    .then(populateLocales)
    .then(populateBranches)
    .then(populateAttribute)
    .then(populateValueOne)
    .then(populateValueTwo)
    // .then(populateValueThree)
    // .then(populateValueFour)

    .then(populateBranches)
    .then(populateAttribute)
    .then(populateValueOne)
    .then(populateValueTwo);
    // .then(populateValueThree)
    // .then(populateValueFour);

  res.send('Complete');
};

exports.seedconfig = (req, res) => {
  let populateHierarchyThemeGroup = function() {
    let promise = new Promise(function(resolve, reject){
      const hierarchyConfig = new FinlitModels.HierarchyConfig({
        name: 'Theme Group',
        attributes: [{
          type: 'text',
          description: 'Title'
        }],
        seq: 0
      });
      hierarchyConfig.collection.drop().then(() => {
      hierarchyConfig.save().then(hierarchyData => {
          console.log('Hierarchy collection saved');
          resolve();
        }).catch((err) =>{
          console.log('Hierarchy collection not saved:', err);
          resolve();
        })
      }).catch((error) => { // Catching drop on empty collection
        console.log(error);
        resolve();
      })
    });
    return promise;
  };
  let populateHierarchyTheme = function() {
    let promise = new Promise(function(resolve, reject){
      const hierarchyConfig = new FinlitModels.HierarchyConfig({
        name: 'Theme',
        attributes: [{
          type: 'text',
          description: 'Title'
        },{
          type: 'textArea',
          description: 'Description',
        }],
        seq: 1
      });
      hierarchyConfig.save().then(hierarchyData => {
          console.log('Hierarchy collection saved');
          resolve();
        }).catch((err) =>{
          console.log('Hierarchy collection not saved:', err);
          resolve();
        })
    });
    return promise;
  };
  populateHierarchyThemeGroup();
  populateHierarchyTheme();
  res.send('Complete');
};





