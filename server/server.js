const jsonServer = require('json-server');
const server = jsonServer.create();
const dbConfig = require('./config/database.config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
server.use(jsonServer.defaults());
server.use(jsonServer.bodyParser);

mongoose.connect(dbConfig.url)
  .then(() => {
    console.log("Successfully connected to the database");
  }).catch(err => {
  console.log('Could not connect to the database. Exiting now...');
  console.log(err);

  process.exit();
});
require('./routes/finlit.routes.js')(server);

server.get('/api', (req, res) => {
  res.json({"message": "Financial Literacy"});
});

const port = process.env.PORT || 3000;

server.listen(port, () => {
  console.log('API running on http://localhost:', port);
});
