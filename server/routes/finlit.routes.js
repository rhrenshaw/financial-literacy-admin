module.exports = (app) => {
  const finlitCtrl = require('../controllers/finlit.controller.js');

  app.post('/auth/login', finlitCtrl.login);
  app.get('/api/users', finlitCtrl.usersGetAllUsers);
  app.get('/api/hierarchy/getnodes', finlitCtrl.nodesGetNodes);
  app.post('/api/hierarchy/getsubactions', finlitCtrl.subactionsGetSubactions);
  app.post('/api/hierarchy/getstreams', finlitCtrl.subactionsGetStreams);
  app.post('/api/hierarchy/updatestreams', finlitCtrl.subactionsUpdateStreams);
  app.post('/api/hierarchy/gettype', finlitCtrl.nodesGetType);
  app.post('/api/hierarchy/updatenode', finlitCtrl.updateNode);
  app.post('/api/hierarchy/deletenode', finlitCtrl.deleteNode);
  app.post('/api/hierarchy/clonenode', finlitCtrl.clonenode);
};
